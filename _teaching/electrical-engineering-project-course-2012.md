---
title: Project Course in Electrical Engineering
course: E1 Project Course in Electrical Engineering
institute: KTH Royal Institute of Technology
year: 2012
term: Spring
lecturer: Cristian R. Rojas
course-url: https://www.kth.se/student/kurser/kurs/EH1010?l=en
---
