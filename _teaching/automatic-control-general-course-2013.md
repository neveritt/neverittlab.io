---
title: Automatic Control, General Course
course: Automatic Control, General Course
institute: KTH Royal Institute of Technology
year: 2013
term: Fall
lecturer: Bo Wahlberg
course-url: https://www.kth.se/student/kurser/kurs/EL1000?l=en_UK
---
