/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icomoon\'">' + entity + '</span>' + html;
	}
	var icons = {
		'icon-school': '&#xe900;',
		'icon-researchgate': '&#xe901;',
		'icon-book': '&#xe91f;',
		'icon-library': '&#xe921;',
		'icon-phone': '&#xe942;',
		'icon-envelop': '&#xe945;',
		'icon-location': '&#xe947;',
		'icon-bubbles': '&#xe96c;',
		'icon-quotes-right': '&#xe978;',
		'icon-wrench': '&#xe991;',
		'icon-trophy': '&#xe99e;',
		'icon-briefcase': '&#xe9ae;',
		'icon-earth': '&#xe9ca;',
		'icon-link': '&#xe9cb;',
		'icon-star-empty': '&#xe9d7;',
		'icon-star-half': '&#xe9d8;',
		'icon-star-full': '&#xe9d9;',
		'icon-heart': '&#xe9da;',
		'icon-new-tab': '&#xea7e;',
		'icon-amazon': '&#xea87;',
		'icon-google': '&#xea88;',
		'icon-google-plus': '&#xea8b;',
		'icon-google-drive': '&#xea8f;',
		'icon-facebook': '&#xea90;',
		'icon-instagram': '&#xea92;',
		'icon-whatsapp': '&#xea93;',
		'icon-spotify': '&#xea94;',
		'icon-telegram': '&#xea95;',
		'icon-twitter': '&#xea96;',
		'icon-vine': '&#xea97;',
		'icon-vk': '&#xea98;',
		'icon-renren': '&#xea99;',
		'icon-sina-weibo': '&#xea9a;',
		'icon-rss': '&#xea9b;',
		'icon-youtube': '&#xea9d;',
		'icon-twitch': '&#xea9f;',
		'icon-vimeo': '&#xeaa0;',
		'icon-lanyrd': '&#xeaa2;',
		'icon-flickr2': '&#xeaa4;',
		'icon-dribbble': '&#xeaa7;',
		'icon-behance': '&#xeaa8;',
		'icon-deviantart': '&#xeaaa;',
		'icon-500px': '&#xeaab;',
		'icon-steam': '&#xeaac;',
		'icon-dropbox': '&#xeaae;',
		'icon-onedrive': '&#xeaaf;',
		'icon-github': '&#xeab0;',
		'icon-npm': '&#xeab1;',
		'icon-basecamp': '&#xeab2;',
		'icon-trello': '&#xeab3;',
		'icon-wordpress': '&#xeab4;',
		'icon-joomla': '&#xeab5;',
		'icon-ello': '&#xeab6;',
		'icon-blogger': '&#xeab7;',
		'icon-tumblr': '&#xeab9;',
		'icon-yahoo': '&#xeabb;',
		'icon-soundcloud': '&#xeac3;',
		'icon-skype': '&#xeac5;',
		'icon-reddit': '&#xeac6;',
		'icon-hackernews': '&#xeac7;',
		'icon-wikipedia': '&#xeac8;',
		'icon-linkedin2': '&#xeaca;',
		'icon-lastfm': '&#xeacb;',
		'icon-delicious': '&#xeacd;',
		'icon-stumbleupon': '&#xeace;',
		'icon-stackoverflow': '&#xead0;',
		'icon-pinterest2': '&#xead2;',
		'icon-xing2': '&#xead4;',
		'icon-flattr': '&#xead5;',
		'icon-foursquare': '&#xead6;',
		'icon-yelp': '&#xead7;',
		'icon-paypal': '&#xead8;',
		'icon-mail': '&#xea83;',
		'icon-rss2': '&#xea9c;',
		'icon-box-add': '&#xe95e;',
		'icon-home': '&#xe902;',
		'icon-camera': '&#xe90f;',
		'icon-headphones': '&#xe910;',
		'icon-music': '&#xe911;',
		'icon-connection': '&#xe91b;',
		'icon-podcast': '&#xe91c;',
		'icon-book2': '&#xe924;',
		'icon-books': '&#xe920;',
		'icon-library2': '&#xe923;',
		'icon-file-text': '&#xe922;',
		'icon-stack': '&#xe92e;',
		'icon-folder': '&#xe92f;',
		'icon-database': '&#xe964;',
		'icon-undo2': '&#xe967;',
		'icon-link2': '&#xe9cc;',
		'icon-attachment': '&#xe9cd;',
		'icon-file-pdf': '&#xeadf;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
