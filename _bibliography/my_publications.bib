---
---
References
==========
@InProceedings{Everitt2013CDC,
  author    = {Niklas Everitt and Cristian R. Rojas and H{\aa}kan Hjalmarsson},
  title     = {A geometric approach to variance analysis of cascaded systems},
  booktitle = {Proceedings of the 52nd IEEE Conference on Decision and Control},
  year      = {2013},
  publisher = {{IEEE}},
  month     = {12},
  doi       = {10.1109/cdc.2013.6760917},
  abstract  = {Modeling complex and interconnected systems is a key issue in
    system identification. When estimating individual subsystems of a network of
    interconnected system, it is of interest to know the improvement of
    model accuracy in using different sensors and actuators. In this paper,
    using a geometric approach, we quantify the accuracy improvement from
    additional sensors when estimating the first of a set of subsystems
    connected in a cascade structure. We present results on how the zeros of
    the first subsystem affect the accuracy of the corresponding model.
    Additionally we shed some light on how structural properties and
    experimental conditions determine the accuracy. The results are
    particularized to FIR systems, for which the results are illustrated by
    numerical simulations. A surprising special case occurs when the first
    subsystem contains a zero on the unit circle; as the model orders grows
    large, the variance of the frequency function estimate, evaluated at the
    corresponding frequency of the unit-circle zero, is shown to be the same as
    if the other subsystems were completely known.},
}

@InProceedings{Everitt2014IFAC,
  author    = {Niklas Everitt and Cristian R. Rojas and H{\aa}kan Hjalmarsson},
  title     = {Variance Results for Parallel Cascade Serial Systems},
  booktitle = {Proceedings of the 19th IFAC World Congress},
  year      = {2014},
  volume    = {47},
  number    = {3},
  month     = {8},
  pages     = {2317--2322},
  doi       = {10.3182/20140824-6-za-1003.01262},
  abstract  = {Modelling dynamic networks is important in different fields of
    science. At present, little is known about how different inputs and sensors
    contribute to the statistical properties concerning an estimate of a
    specific dynamic system in a network. We consider two forms of parallel
    serial structures, one multiple-input-multiple-output structure and one
    single-input-multiple-output structure. The quality of the estimated models
    is analysed by means of the asymptotic covariance matrix, with respect to
    input signal characteristics, noise characteristics, sensor locations and
    previous knowledge about the remaining systems in the network. It is shown
    that an additive property applies to the information matrix for the
    considered structures. The impact of input signal selection, sensor
    locations and incorporation of previous knowledge is illustrated by simple
    examples.},
}

@Article{Martensson2017AUT,
  author    = {Jonas M{\aa}rtensson and Niklas Everitt and H{\aa}kan Hjalmarsson},
  title     = {Covariance analysis in {SISO} linear systems identification},
  journal   = {Automatica},
  year      = {2017},
  volume    = {77},
  month     = {3},
  pages     = {82--92},
  doi       = {10.1016/j.automatica.2016.11.025},
  publisher = {Elsevier {BV}},
  abstract  = {In this paper we analyse the asymptotic covariance of models of
    causal single-input single-output linear time invariant systems. Expressions
    for the asymptotic (co)variance of system properties estimated using the
    prediction error method are derived. These expressions delineate the impacts
    of model structure, model order, true system dynamics, and experimental
    conditions. A connection to results on frequency function estimation is
    established. Also, simple model structure independent upper bounds are
    derived. Explicit variance expressions and bounds are provided for common
    system properties such as impulse response coefficients and non-minimum
    phase zeros. As an illustration of the insights the expressions provide,
    they are used to derive conditions on the input spectrum which make the
    asymptotic variance of non-minimum phase zero estimates independent of
    the model order and model structure.},
}

@Article{Everitt2017AUT,
  author    = {Niklas Everitt and Giulio Bottegal and Cristian R. Rojas and
    H{\aa}kan Hjalmarsson},
  title     = {Variance analysis of linear {SIMO} models with spatially
    correlated noise},
  journal   = {Automatica},
  year      = {2017},
  volume    = {77},
  month     = {3},
  pages     = {68--81},
  doi       = {10.1016/j.automatica.2016.11.017},
  publisher = {Elsevier {BV}},
  abstract  = {Substantial improvement in accuracy of identified linear
    time-invariant single-input multi-output (SIMO) dynamical models is
    possible when the disturbances affecting the output measurements are
    spatially correlated. Using an orthogonal representation for the modules
    composing the SIMO structure, in this paper we show that the variance of a
    parameter estimate of a module is dependent on the model structure of the
    other modules, and the correlation structure of the disturbances. In
    addition, we quantify the variance-error for the parameter estimates for
    finite model orders, where the effect of noise correlation structure, model
    structure and signal spectra are visible. From these results, we derive the
    noise correlation structure under which the mentioned model parameterization
    gives the lowest variance, when one module is identified using less
    parameters than the other modules.},
}

@InProceedings{Everitt2016CDC,
  author    = {Niklas Everitt and Giulio Bottegal and Cristian R. Rojas and
    H{\aa}kan Hjalmarsson},
  title     = {Identification of modules in dynamic networks: An empirical Bayes
    approach},
  booktitle = {Proceedings of the 55th {IEEE} Conference on Decision and Control},
  year      = {2016},
  publisher = {{IEEE}},
  month     = {12},
  doi       = {10.1109/cdc.2016.7798971},
  abstract  = {We address the problem of identifying a specific module in a
    dynamic network, assuming known topology. We express the dynamics by an
    acyclic network composed of two blocks where the first block accounts for
    the relation between the known reference signals and the input to the target
    module, while the second block contains the target module. Using an
    empirical Bayes approach, we model the first block as a Gaussian vector with
    covariance matrix (kernel) given by the recently introduced stable spline
    kernel. The parameters of the target module are estimated by solving a
    marginal likelihood problem with a novel iterative scheme based on the
    Expectation-Maximization algorithm. Numerical experiments illustrate the
    effectiveness of the proposed method.},
}

@InProceedings{Everitt2015CDC,
  author    = {Niklas Everitt and Giulio Bottegal and Cristian R. Rojas and
    H{\aa}kan Hjalmarsson},
  title     = {On the variance analysis of identified linear {MIMO} models},
  booktitle = {Proceedings of the 54th {IEEE} Conference on Decision and Control},
  year      = {2015},
  publisher = {{IEEE}},
  month     = {12},
  doi       = {10.1109/cdc.2015.7402414},
  abstract  = {We study the accuracy of identified linear time-invariant
    multi-input multi-output (MIMO) systems. Under a stochastic framework, we
    quantify the effect of the spatial correlation and choice of model structure
    on the covariance matrix of the transfer function estimates. In particular,
    it is shown how the variance of a transfer function estimate depends on
    signal properties and model orders of other modules composing the MIMO
    system.},
}

@Article{Everitt-submitteda,
  author   = {Niklas Everitt and Giulio Bottegal and H{\aa}kan Hjalmarsson},
  title    = {An empirical Bayes approach to identification of modules in
    dynamic networks},
  journal  = {submitted to Automatica},
  year     = {2017},
  abstract = {We present a new method of identifying a specific module in a
    dynamic network, possibly with feedback loops. Assuming known topology, we
    express the dynamics by an acyclic network composed of two blocks where the
    first block accounts for the relation between the known reference signals
    and the input to the target module, while the second block contains the
    target module. Using an empirical Bayes approach, we model the first block
    as a Gaussian vector with covariance matrix (kernel) given by the recently
    introduced stable spline kernel. The parameters of the target module are
    estimated by solving a marginal likelihood problem with a novel iterative
    scheme based on the Expectation-Maximization algorithm. Additionally, we
    extend the method to include additional measurements downstream of the
    target module. Using Markov Chain Monte Carlo techniques, it is shown that
    the same iterative scheme can solve also this formulation. Numerical
    experiments illustrate the effectiveness of the proposed methods.},
}

@InProceedings{Everitt2015IFAC,
  author    = {Niklas Everitt and Giulio Bottegal and Cristian R. Rojas and
    H{\aa}kan Hjalmarsson},
  title     = {On the Effect of Noise Correlation in Parameter Identification of
    {SIMO} Systems},
  booktitle = {Proceedings of the 17th IFAC Symposium on System Identification},
  year      = {2015},
  volume    = {48},
  number    = {28},
  month     = {10},
  publisher = {Elsevier {BV}},
  pages     = {326--331},
  doi       = {10.1016/j.ifacol.2015.12.148},
  abstract  = {The accuracy of identified linear time-invariant single-input
    multi-output (SIMO) models can be improved when the disturbances affecting
    the output measurements are spatially correlated. Given a linear
    parametrization of the modules composing the SIMO structure, we show that
    the correlation structure of the noise sources and the model structure of
    the othe modules determine the variance of a parameter estimate. In
    particular we show that increasing the model order only increases the
    variance of other modules up to a point. We precisely characterize the
    variance error of the parameter estimates for finite model orders. We
    quantify the effect of noise correlation structure, model structure and
    signal spectra.},
}

@InProceedings{Everitt2017IFAC,
  author    = {Niklas Everitt and Miguel Galrinho and Hjalmarsson, H.},
  title     = {Incorporating noise modeling in dynamic networks using
    non-parametric models},
  booktitle = {Proceedings of the 20th IFAC World Congress},
  date      = {2017},
}

@Article{Everitt-submittedb,
  author   = {Niklas Everitt and Miguel Galrinho and H{\aa}kan Hjalmarsson},
  title    = {Optimal model order reduction with the Steiglitz-McBride method for
    open-loop data},
  journal  = {submitted to Automatica},
  year     = {2017},
  archivePrefix = "arXiv",
  eprint   = {1610.08534},
  abstract = {In system identification, it is often difficult to find a physical
    intuition to choose a noise model structure. The importance of this choice
    is that, for the prediction error method (PEM) to provide asymptotically
    efficient estimates, the model orders must be chosen according to the true
    system. However, if only the plant estimates are of interest and the
    experiment is performed in open loop, the noise model may be
    over-parameterized without affecting the asymptotic properties of the plant.
    The limitation is that, as PEM suffers in general from non-convexity,
    estimating an unnecessarily large number of parameters will increase the
    chances of getting trapped in local minima. To avoid this, a high order ARX
    model can first be estimated by least squares, providing non-parametric
    estimates of the plant and noise model. Then, model order reduction can be
    used to obtain a parametric model of the plant only. We review existing
    methods to perform this, pointing out limitations and connections between
    them. Then, we propose a method that connects favorable properties from the
    previously reviewed approaches. We show that the proposed method provides
    asymptotically efficient estimates of the plant with open loop data.
    Finally, we perform a simulation study, which suggests that the proposed
    method is competitive with PEM and other similar methods.},
}

@Article{Galrinho2017AUT,
  author    = {Miguel Galrinho and Niklas Everitt and H{\aa}kan Hjalmarsson},
  title     = {{ARX} modeling of unstable linear systems},
  journal   = {Automatica},
  year      = {2017},
  volume    = {75},
  month     = {1},
  pages     = {167--171},
  doi       = {10.1016/j.automatica.2016.09.041},
  publisher = {Elsevier {BV}},
  abstract  = {High-order ARX models can be used to approximate a quite general
    class of linear systems in a parametric model structure, and
    well-established methods can then be used to retrieve the true plant and
    noise models from the ARX polynomials. However, this commonly used approach
    is only valid when the plant is stable or if the unstable poles are shared
    with the true noise model. In this contribution, we generalize this approach
    to allow the unstable poles not to be shared, by introducing modifications
    to correctly retrieve the noise model and noise variance.},
}

@Thesis{Everitt2015,
  author   = {Everitt, Niklas},
  title    = {Identification of Modules in Acyclic Dynamic Networks A Geometric
    Analysis of Stochastic Model Errors},
  type     = {Licentiate thesis},
  year     = {2015},
  month    = {2},
  school   = {KTH Royal Institute of Technology},
  abstract = {Systems in engineering are becoming ever more complex and
    interconnected, due to advancing technology with cheaper sensors and
    increased connectivity. For example in process industry, sensors that
    monitor the operation of the plant can be connected through wireless
    connections and used for mon- itoring and control. In this thesis, we study
    the problem of identifying one module, i.e., one transfer function from one
    internal variable to another, in the dynamic network. We investigate how
    accurate models will be obtained using different gathered measurements.
    Model errors are assumed to originate from random disturbances that affect
    the dynamic network. The variance of the model errors are analyzed using the
    classical assumption that a large amount of data is available. By using a
    geometric approach, the (co-)variance of the model errors can be analyzed in
    a way that brings forward how input signal properties, noise variance, noise
    correlation structure and model structure affect the asymptotic model
    errors. Several different network structures are analyzed in order to
    investigate how different signals can reduce the asymp- totic model errors
    in dynamic networks. For SISO systems we develop reparametrization formulas
    for the asymp- totic variance of the model errors of functions of the
    estimated system parameters. In particular, we demonstrate that one can use
    the experimental conditions to make the asymptotic variance independent of
    model order and model structure in some cases. These expressions are used to
    derive simple model structure independent upper bounds of the asymptotic
    covariance for commonly estimated quantities such as system zeros and
    impulse response coefficients. The variance of the first of a set of
    estimated modules connected in a cascade structure is analyzed. The main
    contribution is the characterization of the variance of the frequency
    function estimate of a module with a zero close to the unit circle. It is
    shown that a variance reduction of the first estimated module is possible,
    compared to only using the first measurement in the estimation. The variance
    reduction is concentrated around the frequency of the unit-circle-zero. For
    a parallel cascade structure and a multi sensor structure, upper bounds on
    the asymptotic covariance of the parameter estimates are derived when the
    model order of the system of interest was fixed, while the model order of
    every other module is large. The effect of the noise correlation structure
    is examined for single input multiple output (SIMO) systems. For the case of
    temporally white, but possi- bly spatially correlated additive noise, we
    develop a formula for the asymptotic covariance of the frequency response
    function estimates and a formula for the asymptotic covariance of the model
    parameters. It is shown that when parts of of the noise can be linearly
    estimated from measurements of other blocks with less estimated parameters,
    the variance decreases. The effect of the in- put spectrum is shown to have
    a less significant effect than expected. We determine the optimal
    correlation structure for the noise, for the case when one block has one
    parameter less than the other blocks},
}

@Thesis{Everitt2017,
  author   = {Everitt, Niklas},
  title    = {Module identification in dynamic networks: parametric and
    empirical Bayes methods},
  type     = {PhD thesis},
  year     = {2017},
  month    = {9},
  school   = {KTH Royal Institute of Technology},
  abstract = {The purpose of system identification is to construct mathematical
    models of dynamical system from experimental data. With the current trend of
    dynamical systems encountered in engineering growing ever more complex, an
    important task is to efficiently build models of these systems. Modelling
    the complete dynamics of these systems is in general not possible or even
    desired. However, often, these systems can be modelled as simpler linear
    systems interconnected in a dynamic network. Then, the task of estimating
    the whole network or a subset of the network can be broken down into
    subproblems of estimating one simple system, called module, embedded within
    the dynamic network.

    The prediction error method (PEM) is a benchmark in parametric system
    identification. The main advantage with PEM is that for Gaussian noise, it
    corresponds to the so called maximum likelihood (ML) estimator and is
    asymptotically efficient. One drawback is that the cost function is in
    general nonconvex and a gradient based search over the parameters has to be
    carried out, rendering a good starting point crucial. Therefore, other
    methods such as subspace or instrumental variable methods are required to
    initialize the search. In this thesis, an alternative method, called model
    order reduction Steiglitz-McBride (MORSM) is proposed. As MORSM is also
    motivated by ML arguments, it may also be used on its own and will in some
    cases provide asymptotically efficient estimates. The method is
    computationally attractive since it is composed of a sequence of least
    squares steps. It also treats the part of the network of no direct interest
    nonparametrically, simplifying model order selection for the user.

    A different approach is taken in the second proposed method to identify a
    module embedded in a dynamic network. Here, the impulse response of the part
    of the network of no direct interest is modelled as a realization of a
    Gaussian process. The mean and covariance of the Gaussian process is
    parameterized by a set of parameters called hyperparameters that needs to be
    estimated together with the parameters of the module of interest. Using an
    empirical Bayes approach, all parameters are estimated by maximizing the
    marginal likelihood of the data. The maximization is carried out by using an
    iterative expectation/conditional-maximization scheme, which alternates so
    called expectation steps with a series of conditional-maximization steps.
    When only the module input and output sensors are used, the expectation step
    admits an analytical expression. The conditional-maximization steps reduces
    to solving smaller optimization problems, which either admit a closed form
    solution, or can be efficiently solved by using gradient descent strategies.
    Therefore, the overall optimization turns out computationally efficient.
    Using markov chain monte carlo techniques, the method is extended to
    incorporate additional sensors.

    Apart from the choice of identification method, the set of chosen signals to
    use in the identification will determine the covariance of the estimated
    modules. To chose these signals, well known expressions for the covariance
    matrix could, together with signal constraints, be formulated as an
    optimization problem and solved. However, this approach does neither tell us
    why a certain choice of signals is optimal nor what will happen if some
    properties change. The expressions developed in this part of the thesis have
    a different flavor in that they aim to reformulate the covariance
    expressions into a form amenable for interpretation. These expressions
    illustrate how different properties of the identification problem affects
    the achievable accuracy. In particular, how the power of the input and noise
    signals, as well as model structure, affect the covariance.},
}
