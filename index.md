---
# You don't need to edit this file, it's empty on purpose.
# Edit theme's home layout instead if you wanna make some changes
# See: https://jekyllrb.com/docs/themes/#overriding-theme-defaults
layout: mypage
title: About
---

I received my M.Sc. in Engineering Physics in 2012 from Lund University.
I spent the academic year $$2009/2010$$ at University of Illinois
Urbana Champaign, and have been a visiting research scholar at TU Eindhoven.
In 2012 I joined the Department of Automatic Control at KTH as a PhD student
under the supervision of Professor [Håkan Hjalmarsson][hjalmars] and
Associate professor [Cristian R. Rojas][crro].

[crro]: https://www.kth.se/profile/crro/
[hjalmars]: https://www.kth.se/profile/hjalmars

### Interests
- Automatic Control
- System Identification
- Signal Processing
