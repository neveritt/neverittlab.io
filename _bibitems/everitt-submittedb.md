---
---
@Article{Everitt-submittedb,
  author  = {Niklas Everitt and Miguel Galrinho and H{\aa}kan Hjalmarsson},
  title   = {Optimal model order reduction with the Steiglitz-McBride method for open-loop data},
  journal = {submitted to Automatica},
  year    = {2017},
}
