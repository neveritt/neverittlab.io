---
---
@InProceedings{Everitt2015CDC,
  author    = {Niklas Everitt and Giulio Bottegal and Cristian R. Rojas and H{\aa}kan Hjalmarsson},
  title     = {On the variance analysis of identified linear {MIMO} models},
  booktitle = {Proceedings of the 54th {IEEE} Conference on Decision and Control},
  year      = {2015},
  publisher = {{IEEE}},
  month     = {12},
  doi       = {10.1109/cdc.2015.7402414},
}
