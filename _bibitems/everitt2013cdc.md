---
---
@InProceedings{Everitt2013CDC,
  author    = {Niklas Everitt and Cristian R. Rojas and H{\aa}kan Hjalmarsson},
  title     = {A geometric approach to variance analysis of cascaded systems},
  booktitle = {Proceedings of the 52nd IEEE Conference on Decision and Control},
  year      = {2013},
  publisher = {IEEE},
  month     = {12},
  doi       = {10.1109/cdc.2013.6760917},
}
