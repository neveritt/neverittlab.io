---
---
@Article{Everitt-submitteda,
  author  = {Niklas Everitt and Giulio Bottegal and H{\aa}kan Hjalmarsson},
  title   = {An empirical Bayes approach to identification of modules in dynamic networks},
  journal = {submitted to Automatica},
  year    = {2017},
}
