---
---
@Thesis{Everitt2015,
  author = {Everitt, Niklas},
  title  = {Identification of Modules in Acyclic Dynamic Networks A Geometric Analysis of Stochastic Model Errors},
  type   = {Licentiate thesis},
  year   = {2015},
  month  = {2},
  school = {KTH Royal Institute of Technology},
}
