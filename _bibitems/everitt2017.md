---
---
@Thesis{Everitt2017,
  author = {Everitt, Niklas},
  title  = {Module identification in dynamic networks: parametric and empirical Bayes methods},
  type   = {PhD thesis},
  year   = {2017},
  month  = {9},
  school = {KTH Royal Institute of Technology},
}
