---
---
@Article{Martensson2017AUT,
  author    = {Jonas M{\aa}rtensson and Niklas Everitt and H{\aa}kan Hjalmarsson},
  title     = {Covariance analysis in {SISO} linear systems identification},
  journal   = {Automatica},
  year      = {2017},
  volume    = {77},
  month     = {3},
  pages     = {82--92},
  doi       = {10.1016/j.automatica.2016.11.025},
  publisher = {Elsevier {BV}},
}
