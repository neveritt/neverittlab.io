---
---
@InProceedings{Everitt2015IFAC,
  author    = {Niklas Everitt and Giulio Bottegal and Cristian R. Rojas and H{\aa}kan Hjalmarsson},
  title     = {On the Effect of Noise Correlation in Parameter Identification of {SIMO} Systems},
  booktitle = {Proceedings of the 17th IFAC Symposium on System Identification},
  year      = {2015},
  volume    = {48},
  number    = {28},
  publisher = {Elsevier {BV}},
  pages     = {326--331},
  doi       = {10.1016/j.ifacol.2015.12.148},
}
