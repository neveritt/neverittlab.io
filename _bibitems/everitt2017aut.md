---
---
@Article{Everitt2017AUT,
  author    = {Niklas Everitt and Giulio Bottegal and Cristian R. Rojas and H{\aa}kan Hjalmarsson},
  title     = {Variance analysis of linear {SIMO} models with spatially correlated noise},
  journal   = {Automatica},
  year      = {2017},
  volume    = {77},
  month     = {3},
  pages     = {68--81},
  doi       = {10.1016/j.automatica.2016.11.017},
  publisher = {Elsevier {BV}},
}
