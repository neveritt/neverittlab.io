---
---
@Article{Galrinho2017AUT,
  author    = {Miguel Galrinho and Niklas Everitt and H{\aa}kan Hjalmarsson},
  title     = {\{ARX\} modeling of unstable linear systems},
  journal   = {Automatica},
  year      = {2017},
  volume    = {75},
  month     = {1},
  pages     = {167--171},
  doi       = {10.1016/j.automatica.2016.09.041},
  publisher = {Elsevier {BV}},
}
