---
---
@InProceedings{Everitt2017IFAC,
  author    = {Niklas Everitt and Miguel Galrinho and Hjalmarsson, H.},
  title     = {Incorporating noise modeling in dynamic networks using non-parametric models},
  booktitle = {Proceedings of the 20th IFAC World Congress},
  date      = {2017},
}
