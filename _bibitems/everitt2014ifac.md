---
---
@InProceedings{Everitt2014IFAC,
  author    = {Niklas Everitt and Cristian R. Rojas and H{\aa}kan Hjalmarsson},
  title     = {Variance Results for Parallel Cascade Serial Systems},
  booktitle = {Proceedings of the 19th IFAC World Congress},
  year      = {2014},
  volume    = {47},
  number    = {3},
  pages     = {2317--2322},
  doi       = {10.3182/20140824-6-za-1003.01262},
}
