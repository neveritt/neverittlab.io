---
---
@InProceedings{Everitt2016CDC,
  author    = {Niklas Everitt and Giulio Bottegal and Cristian R. Rojas and H{\aa}kan Hjalmarsson},
  title     = {Identification of modules in dynamic networks: An empirical Bayes approach},
  booktitle = {Proceedings of the 55th {IEEE} Conference on Decision and Control},
  year      = {2016},
  publisher = {{IEEE}},
  month     = {12},
  doi       = {10.1109/cdc.2016.7798971},
}
