---
layout: page
title: Bibliography
menu: true
---

[Journal papers](#journal-papers)
[Conference papers](#conference-papers)
[Theses](#theses)

Journal papers
------------------------

{% bibliography --query @article %}

Conference papers
------------------------

{% bibliography --query @inproceedings %}

Theses
------------------------

{% bibliography --query @thesis %}
